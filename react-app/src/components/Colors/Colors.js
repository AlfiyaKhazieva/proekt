const colors = [
    {
        id: 1,
        image: 'img/color-1.webp',
        alt: 'айфон красного цвета'
    },
    {
        id: 2,
        image: 'img/color-2.webp',
        alt: 'айфон зеленого цвета'
    },
    {
        id: 3,
        image: 'img/color-3.webp',
        alt: 'айфон розового цвета'
    },
    {
        id: 4,
        image: 'img/color-4.webp',
        alt: 'айфон синего цвета'
    },
    {
        id: 5,
        image: 'img/color-5.webp',
        alt: 'айфон голубого цвета'
    },
    {
        id: 6,
        image: 'img/color-6.webp',
        alt: 'айфон черного цвета'
    },
]

export default colors;