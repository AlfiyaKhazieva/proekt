import './Colors.css';
import colors from './Colors';
import React, { useState } from 'react';

function ColorButton() {
    const [selectedColor, setSelectedColor] = useState(4);
    return (
        <div className="product-selection">
            {colors.map(function(color) {
                 let actived = selectedColor === color.id;
                let activeClass = actived ? 'checked': '';
                return (
                <div className={`product-selection__label ${activeClass}`} key={`color_${color.id}`}>
                    <input
                     id={`color-button-${color.id}`} 
                     type="radio"  
                     name="radio" 
                     checked={actived}
                     onChange={() => {}}
                     onClick={() =>setSelectedColor(color.id)} 
                     />
                    <label htmlFor={`color-button-${color.id}`}><img className="product-selection__img" src={color.image} alt={color.alt}/></label> 
                </div>
            )})}
        </div>
    )
}


export default ColorButton;