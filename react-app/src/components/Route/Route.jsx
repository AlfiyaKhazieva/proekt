import React from 'react';
import { Routes, Route, BrowserRouter } from 'react-router-dom';
import PageProduct from '../PageProduct/PageProduct';
import PageProductToo from '../PageProductToo/PageProductToo';


export const ContentRouter = () => (
    <BrowserRouter basename="/">
    <Routes>
    <Route path="/*" element={<PageProduct />} />
    <Route path="/test" element={<PageProductToo />} />
    </Routes>
  </BrowserRouter>
);