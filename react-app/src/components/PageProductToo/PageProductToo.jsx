import { Link } from "react-router-dom";
import './PageProductToo.css'

function PageProductToo () {
    return(
        <>
        <div className="container">
            <main className="main">
                <div className="main__text">
                <Link to="/">Вернуться на страницу товара</Link>
                </div>
            </main>
        </div>
        </>
    )
}

export default PageProductToo;