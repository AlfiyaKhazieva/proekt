import './Footer.css';

function Footer() {
    return (
        <footer className="footer">
            <div className="container">
                <div className="footer__content">
                    <div>
                        <p className="footer__text"><b>© ООО «<span className="website">Мой</span>Маркет», 2018-2022</b> <br/> Для уточнения информации звоните по номеру <a className="navigation__link ref" href="tel:+7 900 000 0000">+7 900 000 0000</a>,<br/>а предложения по сотрудничеству отправляйте на почту <a className="navigation__link ref" href="mailto:partner@mymarket.com">partner@mymarket.com</a></p>
                    </div>
                    <div>
                        <a className="navigation__link" href="#top">Наверх</a>
                    </div>
                </div>
            </div>
        </footer>
    );
}

export default Footer;