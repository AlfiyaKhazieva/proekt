import { configureStore, } from "@reduxjs/toolkit";
import LikeReducer from "../LikeReducer/LikeReducer";
import CartReducer from '../CartReducer/CartReducer';



export const store = configureStore({
    reducer: {
        cart: CartReducer,
        like: LikeReducer,
    },

});


export default store;