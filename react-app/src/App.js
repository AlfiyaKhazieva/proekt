import './App.css';
import PageProduct from './components/PageProduct/PageProduct';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import { ContentRouter } from './components/Route/Route';

function App() {
  return (
    <>
      <Header />
      <ContentRouter />
      <Footer />
    </>
  );
}

export default App