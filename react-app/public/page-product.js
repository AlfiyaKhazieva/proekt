let inputName = document.querySelector(".form__name");
let inputNumber = document.querySelector(".form__number");
let textArea = document.querySelector(".form__review");
let form = document.querySelector('.form');
let errorName = document.getElementById('error__name__requered');
let errorLength = document.getElementById('error__name__length');
let errorGrade = document.getElementById('error__grade');


// Сохроняем введеные данные в поле ввода после обновления cтраницы
inputName.value = localStorage.getItem('inputName');
inputNumber.value = localStorage.getItem('inputNumber');
textArea.value = localStorage.getItem('textArea');


function isFormValidate () {
    if(inputName.value === '') {
        errorName.classList.add('error__error');
        inputName.classList.add('error-border');
        return false;
    } 

    if (inputName.value.length < 2) {
        errorLength.classList.add('error__error');
        inputName.classList.add('error-border');
        return false;
    }
    

    if (+inputNumber.value === '') {
        errorGrade.classList.add('error__error');
        inputNumber.classList.add('error-border');
        return false;
    }

    if(isNaN(+inputNumber.value)) {
        errorGrade.classList.add('error__error');
        inputNumber.classList.add('error-border');
        return false;
    }

    if(+(inputNumber.value > 5) || (inputNumber.value < 1)) {
        errorGrade.classList.add('error__error');
        inputNumber.classList.add('error-border');
        return false;
    }
    return true;
}


form.addEventListener('submit', function(event){
    event.preventDefault ();

    if (isFormValidate ()) {
        //Очистить все из localStorage
        localStorage.clear();
    }
});



inputName.addEventListener('input', function(event) {
    errorName.classList.remove('error__error');
    errorLength.classList.remove('error__error');
    inputName.classList.remove('errror-border');

    localStorage.setItem('inputName', event.target.value)
});




inputNumber.addEventListener('input', function(event) {
    errorGrade.classList.remove('error__error');
    inputNumber.classList.remove('errror-border');

    localStorage.setItem('inputNumber', event.target.value)
});



textArea.addEventListener('input', function(event){
    console.log(event.target.value);

    localStorage.setItem('textArea', event.target.value)
});







