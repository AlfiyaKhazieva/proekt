// Упражнение 1

// с первым дополнительным условием, если пользовтаель вводит не число 

let userCount = +prompt('Введите число', '');
// (Number(userCount) === NaN) --- неправильная проверка на NaN, используйте функцию isNaN()
if (isNaN(userCount)) {
    // Введена строка - выводим ошибку
    console.log('Ошибка, вы ввели не число');
} else {
    // Введено число - запускаем таймер
       
    let count = userCount;
    
    let interval = setInterval(function () {
        console.log(`Осталось ${count}`);
        count = count - 1;
    
        if (count === 0) {
            clearInterval(interval);
            console.log('Время вышло');
        }
    }, 1000);
}


// Упражнение 2

let promise = fetch('https://reqres.in/api/users');

promise
  .then((response) => {
    return response.json();
  })
  .then((result) => {
    console.log(`Получили пользователей ${result.data.length}:`);
    result.data.forEach((item) => {
        console.log(`- ${item.first_name} ${item.last_name} (${item.email})`);
    });
  });
 