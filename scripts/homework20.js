// Упражнение 1

for ( let i = 0; i<=20; i++) {
   if (i % 2 == 0) {
    console.log(i);
   }
}

// ИЛИ

let b = 0
while (b <= 20) {
    if (b % 2 == 0) {
        console.log(b);
    } b++;
}

// Упражнение 2

let sum = 0;
isErrors = false;

for (let i = 0; i < 3; i++) {
     let num = +prompt ('Введите число', '');

    if (!num) {
         isErrors = true;
         alert('Ошибка, вы ввели не число');
         break; 
    }

    sum = sum + num;
}

if (!isErrors) {
     alert('Сумма: '+ sum); 
}    

// Упражнение 3

let month = +prompt ('Ведите название месяца', '');

function getNameOfMonth(month) {
    switch (month) {
        case 0:
            alert('Январь');
            break;
        case 1:
            alert('Февраль');
            break;
        case 2:
            alert('Март');
            break;
        case 3:
            alert('Апрель');
            break;
        case 4:
            alert('Май');
            break;
        case 5:
            alert('Июнь');
            break;
        case 6:
            alert('Июль');
            break;
        case 7:
            alert('Август');
            break;
        case 8:
            alert('Сентябрь');
            break;
        case 9:
            alert('Октябрь');
            break;
        case 10:
            alert('Ноябрь');
            break;
        case 11:
            alert('Декабрь');
            break;
        default:
            alert('Нет таких значений')

    }

}

alert(getNameOfMonth(month));

// ИЛИ

function getNameOfMonth(n) {
    if (n === 0) return 'Январь';
    if (n === 1) return 'Февраль';
    if (n === 2) return 'Март';
    if (n === 3) return 'Апрель';
    if (n === 4) return 'Май';
    if (n === 5) return 'Июнь';
    if (n === 6) return 'Июль';
    if (n === 7) return 'Август';
    if (n === 8) return 'Сентябрь';
    if (n === 9) return 'Октябрь';
    if (n === 10) return 'Ноябрь';
    if (n === 11) return 'Декабрь';
}

console.log(getNameOfMonth(0))


for (let n = 0; n < 12; n++) {
    
    let month =  getNameOfMonth(n);

    if (month == 'Октябрь') continue;

    console.log(month);

}
