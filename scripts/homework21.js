// Упражнение 1

let user = {}

alert(isEmpty(user));

user.age = 12;

alert(isEmpty(user));


/**
 * Проверка объекта на наличие свойств 
 * @param {object} obj - проверяемый объект 
 * @returns {boolean} возвращает результат функции, в зависмости от выполнения
 */

function isEmpty(obj) {
    for (let key in obj) {
     return false;
    }
  return true;
  
  }  

console.log(isEmpty(user));

// Упражнение 3

let salaries = { 
    John: 100000,
    Ann: 160000,
    Pete: 130000,

}

let perzent = 5;

/**
 * Функция, предназначенная для повышения зарплаты на 5%
 * @param {number} perzent - процент повышения зарплаты 
 * @returns {object} - возвращает новый объект 
 */

function raiseSalary(perzent) {
    
    newSalaries = {}

    for (let key in salaries) {
        newSalaries[key] = salaries[key] + Math.floor(salaries[key] * (perzent/100));
    }

    return newSalaries;
}

console.log(raiseSalary(perzent));

/**
 * Функция, предназначенная для подсчета суммы всех зарплат сотрудников, после повышения 
 * @param {object} - проверяемый объект
 * @returns {number} - возращает сумму все сотрудников 
 */

function calcSumm(obj) {
    
    let summ = 0;

    for (let key in obj) {
        summ = summ + obj[key];
    }
    
    return summ;

}

console.log(calcSumm(raiseSalary(perzent)));




