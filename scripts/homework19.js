// Упражнение 1

let a = '$100'
let b = '300$'

let summ = +a.slice(1) + +b.slice(0, 3); // Ваше решение

console.log(summ); // Должно быть 400

// Управжение 2

let message = ' привет, медвед      ';

message = message.trim()  // Решение должно быть написано тут

console.log(message[0].toUpperCase() + message.slice(1)); // “Привет, медвед”

// Упражнение 3

let age = prompt ('Сколько вам лет?', '');

if (age<=3) {
    alert( `Вам ${age} и вы младенец` );
} else if (age<=11) {
    alert( `Вам ${age} и вы ребенок` );
} else if (age<=18) {
    alert( `Вам ${age} и вы подросток` );
} else if (age<=40) {
    alert( `Вам ${age} и вы познаете жизнь` );
} else if (age<=80) {
    alert( `Вам ${age} и вы познали жизнь` );
} else {
    alert( `Вам ${age} и вы долгожитель` );
}

// Упражнение 4

let mesage = 'Я работаю со строками как профессионал!' ;

mesage = mesage.split(' ');

let count = mesage.length; // Ваше решение

console.log(count); // Должно быть 6




