// Валидация формы

'use strict';
 
let form = document.querySelector('.form');
let inputName = document.querySelector('.form__name');
let inputNumber = document.querySelector('.form__number');
let textArea = document.querySelector('.form__review');
let button = document.querySelector('.btn.form__btn');

let errorName = document.getElementById('error__name_requered');
let errorLength = document.getElementById('error__name_length');
let errorGrade = document.getElementById('error_grade');

// Дефолтные значения полей беруться из storag
inputName.value = localStorage.getItem('inputName');
inputNumber.value = localStorage.getItem('inputNumber');
textArea.value = localStorage.getItem('textArea');



function isFormValidate() {
    if (inputName.value === ''){
        errorName.classList.add('error-error');
        inputName.classList.add('error-border');
        return false;
    }
    if (inputName.value.length<2){
        errorLength.classList.add('error-error');
        inputName.classList.add('error-border');
        return false;
    };


    if (inputNumber.value === '') {
        errorGrade.classList.add('error-error');
        inputNumber.classList.add('error-border');
        return false;
    } 
    if (isNaN(Number(inputNumber.value))) {
        errorGrade.classList.add('error-error');
        inputNumber.classList.add('error-border');
        return false;
    }
    if (Number(inputNumber.value) > 5 || Number(inputNumber.value) < 1) {
        errorGrade.classList.add('error-error');
        inputNumber.classList.add('error-border');
        return false
    }

    return true;
}


form.addEventListener('submit', function(event) {
    event.preventDefault();

    if (isFormValidate()) {
        // Очитстить все localStorage
        localStorage.clear();

        inputName.value = '';
        inputNumber.value = '';
        textArea.value = '';
    }
});

inputName.addEventListener('input', function(event) {
    errorName.classList.remove('error-error');
    errorLength.classList.remove('error-error');

    inputName.classList.remove('error-border');

    localStorage.setItem('inputName', event.target.value);
});

inputNumber.addEventListener('input', function(event) {
    errorGrade.classList.remove('error-error'); 

    inputNumber.classList.remove('error-border');

    localStorage.setItem('inputNumber', event.target.value);
});


textArea.addEventListener('input', function(event) {
    localStorage.setItem('textArea', event.target.value);
});



// Счетчик добавления в корзину

//Кнопка добавить в корзину
let asideBtn = document.querySelector('.aside__button');

// Счетчик на корзине
let countBacket = document.querySelector('.header__products');

// Сохряем введенные данные счетчика после обновления страинцы 
let contBas = +localStorage.getItem('countBacket');



// asideBtn присваеваем событие клик
asideBtn.addEventListener('click', function(event) {
    //countBacket.innerText = ++countBacket.innerText;
    //countBacket.classList.add('counter__basket');

    if (contBas === 0 ) {
        asideBtn.innerText = 'Товар уже в корзине';
        asideBtn.style.background = '#888888';
        countBacket.classList.add('counter__basket');
        countBacket.innerHTML = `${contBas + 1 }`;
        contBas++;
        localStorage.setItem('countBacket', contBas);
        return;

        
    } else  {
        asideBtn.innerText = 'Добавить в коризну';
        countBacket.classList.remove('counter__basket'); 
        asideBtn.style.background = '';
        contBas--;
        localStorage.setItem('countBacket', contBas);
    }

});


let basketCounter = function (event) {
    if (event === 0) {
        return;
    } else {
        countBacket.classList.add('counter__basket');
        asideBtn.innerText = 'Товар уже в корзине';
        asideBtn.style.background = '#888888';
        countBacket.innerHTML = `${contBas}`;
    }

};

basketCounter(contBas);








