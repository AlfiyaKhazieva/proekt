// Управжнение 1

let arr1 = [1,2,10,5];

let arr2 = ["a", {}, 3, 3, -2];

function getSumm(array) {

    let sum = 0;
    
    let arr = array.filter(function(v) {
        if (typeof v === 'number') {
        return true;
        }
    });

    for (let i = 0; i < arr.length; i++) {
        sum = sum + arr[i];
    }

    return sum;

}
alert(getSumm(arr1)); // 18
alert(getSumm(arr2)); // 4 


// Упражнение 3

let cart = new Set([4884]);

function addToCart(product) {
    cart.add(product);

}

function removeFromCart(product) {
    cart.delete(product);
}


// В корзине один товар
//let cart = [4884];

// Добавили товар 
addToCart(3456);
console.log(cart); // [4884, 3456]

// Повторно добавили товар
addToCart(3456);
console.log(cart); // [4884, 3456]

// Удалили товар
removeFromCart(4884);
console.log(cart); // [3456]
